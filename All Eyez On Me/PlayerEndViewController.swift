

/***
 **Module Name:  PlayerEndViewController.
 **File Name :  PlayerEndViewController.swift
 **Project :   theprogrampictures.com
 **Copyright(c) : Program Pictures.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Next asset Details.
 */

import UIKit

protocol playerEndDelegate:class{
    func playEnd(userId: String, videoId: String, deviceId: String, MyList: Bool,videoURL:String)
    
     func addrecentwatchList(dict:NSDictionary)
}

class PlayerEndViewController: UIViewController {
    
    var nextCollectionList = NSMutableArray()
    var nextrowID = Int()
    var userID = String()
    var deviceID = String()
    var nextData = NSDictionary()
    var fromsearch  = Bool()
    var videoDict = NSDictionary()

    @IBOutlet weak var bgImg: UIImageView!
  
    @IBOutlet weak var titleLbl: UILabel!
   
    @IBOutlet weak var automated: UILabel!
    @IBOutlet weak var desLbl: UILabel!
    @IBOutlet weak var thumbImg: UIImageView!

    @IBOutlet weak var duration: UILabel!
    var myTimer = Timer()
    var i = Int()
    var playEnddelegate:playerEndDelegate?
    var videoUrl = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        i = 5
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleMenuPress))
        tapGesture.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
        self.view.addGestureRecognizer(tapGesture)
   
        let metaData = nextData[kMetadata] as! NSDictionary
        let bgImage = isnil(json: metaData, key: "main_carousel_image_url")
        let thumbImage = isnil(json: metaData, key: "movie_art")
        bgImg.kf.setImage(with: URL(string: bgImage))
        thumbImg.kf.setImage(with: URL(string: thumbImage))
        titleLbl.text = isnil(json: nextData, key: "name")
        desLbl.text = isnil(json: nextData, key: "description")
        let dur = isnil(json: nextData, key: "file_duration")
        duration.text = stringFromTimeInterval(interval: Double(dur)!)
    }

    func handleMenuPress()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let DetailVC = storyBoard.instantiateViewController(withIdentifier: "DetailPage") as! DetailPageViewController

        for viewcontroller in (self.navigationController?.viewControllers)!
        {
            if viewcontroller.isKind(of: DetailPageViewController.self)
            {
                 let  _ =  self.navigationController?.pushViewController(DetailVC, animated: false)
            }
        }
    }
    func getData(getnextData:NSDictionary)
    {
        self.nextData = getnextData
  
        RecentlyWatched(withurl:kRecentlyWatchedUrl)
        myTimer = Timer.scheduledTimer(timeInterval: 1.1, target: self, selector: #selector(self.getplay), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        i = 5
    }
    func getplay()
    {
   
        automated.text = "\(String(i)) SEC"
        i = i-1
        if i == 0
        {
          myTimer.invalidate()
        
          playvideo()
        }
        
    }
    
    @IBAction func playButton(_ sender: Any) {
        myTimer.invalidate()
        playvideo()
    }
    
    func playvideo()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let PlayerVC = storyBoard.instantiateViewController(withIdentifier: "playerLayer") as! PlayerLayerViewController
      
            PlayerVC.isResume = false
            if nextData["url_m3u8"] != nil && nextData["url_m3u8"] as! String != ""
            {
               videoUrl = nextData["url_m3u8"] as! String
            }
            else
            {
               videoUrl = nextData["url"] as! String
            }
            PlayerVC.mainVideoID = nextData["id"] as! String
            PlayerVC.userID = userID
            PlayerVC.deviceID = deviceID
            PlayerVC.videoID = (nextData["id"] as! String)
            PlayerVC.isMyList = false
            PlayerVC.videoUrl = self.videoUrl
            self.playEnddelegate?.playEnd(userId: self.userID, videoId: nextData["id"] as! String, deviceId: self.deviceID, MyList: false,videoURL: self.videoUrl)
//            if fromsearch
//            {
//                self.present(PlayerVC, animated: false, completion: nil)
//            }
//            else
//            {
              //
          //   }
//        for viewcontroller in (self.navigationController?.viewControllers)!
//        {
//            if viewcontroller.isKind(of: PlayerLayerViewController.self)
//            {
//                let  _ =  self.navigationController?.pushViewController(PlayerVC, animated: false)
//            }
//        }
        let _ = self.navigationController?.popViewController(animated: true)
    }
    func RecentlyWatched(withurl:String)
    {
        let parameters = ["createRecentlyWatched": ["videoId":nextData["id"] as! String,"userId":userID as AnyObject,"carouselName":"All Eyez On Me" as AnyObject]]
   
        ApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters as [String : [String : AnyObject]]){(responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict
                let dict = JSON as! NSDictionary
                let dicton = dict["recentlyWatched"] as! NSDictionary
                if (dicton["seekTime"] as! Float64) > 0
                {
                  
                }
                self.playEnddelegate?.addrecentwatchList(dict: dicton)
            }
            else
            {
                print("json error")
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                let _ = self.navigationController?.popViewController(animated: true)
//                })
//                alertview.addAction(defaultAction)
//                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
