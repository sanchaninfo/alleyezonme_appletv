

/***
 **Module Name: DetailPageViewController
 **File Name :  DetailPageViewController.swift
 **Project :   theprogrampictures.com
 **Copyright(c) : Program Pictures.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Asset Details.
 */

import UIKit
import AVKit

protocol myListdelegate: class
{
    func myListdata(mylistDict:NSDictionary)
    func removeListdata(id: String)
    func recentlyWatcheddata(recentlyWatchedDict:NSDictionary)
}
class DetailPageViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,playerdelegate {
    
    var EpisodesPath = [[String:Any]]()
    var TvshowPath = NSDictionary()
    var userid = String()
    var deviceId,uuid: String!
    var Menus = [String]()
    var isMain = Bool()
    var isRecentlywatch = Bool()
    var iswatchedVideo = Float64()
    var seektime = Float64()
    var tvshowCount = Int()
    var seektimevideo = Float64()
    var subscription_status = String()
    var statusDict = NSDictionary()
    var isMyList = Bool()
    var VideoID = String()
    var dataUrl = String()
    var isUpdated = Bool()
    var fromSearch = Bool()
    var tvshow = Bool()
    var ispaired = Bool()
 
    weak var delegate:myListdelegate?
    
    @IBOutlet weak var BackgrndImg: UIImageView!
    @IBOutlet weak var TvLbl: UILabel!
    @IBOutlet weak var TimeLbl: UILabel!
    @IBOutlet weak var ReleaseLbl: UILabel!
    @IBOutlet weak var descriptiontxt: UITextView!
    @IBOutlet var operationsListView:UITableView!
    @IBOutlet var starView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //accountStatus()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         getaccountDetails()
//        if (TvshowPath["contains"] != nil)
//        {
//            if ((TvshowPath["contains"] as! NSArray).count != 0)
//            {
//                tvshowCount = getTvShowCount(isTvShow: true)
//                tvshow = true
//            }
//            else
//            {
//                tvshowCount = getTvShowCount(isTvShow: false)
//                tvshow = false
//            }
//        }
//        else
//        {
            tvshowCount = getTvShowCount(isTvShow: false)
            tvshow = false
     //   }
        starView.isUserInteractionEnabled = false
        BackgrndImg.kf.indicatorType = .activity
        let metaDict = TvshowPath.object(forKey: "metadata") as! NSDictionary
    
        BackgrndImg.kf.setImage(with: URL(string: isnil(json: metaDict, key: "main_carousel_image_url")))
        TvLbl.text = (isnil(json:TvshowPath,key:"name")).capitalized
        let labelText = TvLbl.text?.capitalized
        TvLbl?.frame = CGRect(x: (TvLbl?.frame.origin.x)!, y: (TvLbl?.frame.origin.y)!, width: (labelText?.widthWithConstrainedWidth(height: 60, font: (TvLbl?.font)!))!, height: (TvLbl?.frame.size.height)!)
        TimeLbl.text =  stringFromTimeInterval(interval: Double(isnil(json:TvshowPath,key:"file_duration"))!)
        let str1 = (isnil(json: metaDict, key: "release_date")).components(separatedBy: "-")
        ReleaseLbl.text = str1[0]
        descriptiontxt.text = isnil(json: TvshowPath, key: "description")
        
        let DescriptionText = TvshowPath["description"] as? String
        let destxt = DescriptionText?.replacingOccurrences(of: "&", with: "", options: .literal, range: nil)
        let destxt1 =  destxt?.replacingOccurrences(of: "amp", with: "", options: .literal, range: nil)
        let destxt2 = destxt1?.replacingOccurrences(of: ";", with: "", options: .literal, range: nil)
        descriptiontxt.text = destxt2
        descriptiontxt?.frame = CGRect(x: (descriptiontxt?.frame.origin.x)!, y: (descriptiontxt?.frame.origin.y)!, width: (descriptiontxt?.frame.size.width)!, height: (DescriptionText?.widthWithConstrainedWidth(height: 60, font: (descriptiontxt?.font)!))!)
        operationsListView.reloadData()
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tvshowCount
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        (cell.viewWithTag(11) as! UILabel).text = Menus[indexPath.row]
        (cell.viewWithTag(11) as! UILabel).textColor = UIColor.white
        cell.selectionStyle = .none
        cell.layer.cornerRadius = 7.0
        //Seektime > 0
        if TvshowPath["watchedVideo"] != nil
        {
            if  TvshowPath["watchedVideo"] as! Float64 > 0.0
            {
                switch indexPath.row
                {
                case 0:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                    break
                case 1:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                    break
                case 2:
                    if TvshowPath["myList"] as! Bool == false
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList")
                    }
                    break
                case 3:
                    if tvshow == true
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                    }
                    break
                case 4:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                    break
                default:
                    break
                }
            }
            else
            {
                switch indexPath.row
                {
                case 0:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                    break
                case 1:
                    if TvshowPath["myList"] as! Bool == false
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList")
                    }
                    break
                case 2:
                    if tvshow == true
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                    }
                    break
                case 3:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                default:
                    break
                }
            }
        }
      
            //  Seektime < 0
        else
        {
            switch indexPath.row
            {
            case 0:
                (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                break
            case 1:
                if TvshowPath["myList"] as! Bool == false
                {
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist")
                }
                else
                {
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList")
                }
                break
            case 2:
                if tvshow == true
                {
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                }
                else
                {
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                }
                break
            case 3:
                (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
            default:
                break
            }
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        // previous focus view
        if let previousIndexPath = context.previouslyFocusedIndexPath,
            let cell = tableView.cellForRow(at: previousIndexPath)
        {
            (cell.viewWithTag(11) as! UILabel).textColor = UIColor.white
            if TvshowPath["watchedVideo"] != nil
            {
                if  TvshowPath["watchedVideo"] as! Float64 > 0.0
                {
                    switch previousIndexPath.row
                    {
                    case 0:
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                        break
                    case 1:
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                        break
                    case 2:
                        if (cell.viewWithTag(11) as! UILabel).text == "Add To MyList"
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist")
                        }
                        else
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList")
                        }
                        break
                    case 3:
                        if tvshow == true
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                        }
                        else
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                        }
                        break
                    case 4:
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                        break
                    default:
                        break
                    }
                }
                else
                {
                    switch previousIndexPath.row
                    {
                    case 0:
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                        break
                    case 1:
                        if (cell.viewWithTag(11) as! UILabel).text == "Add To MyList"
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist")
                        }
                        else
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList")
                        }
                        break
                    case 2:
                        if tvshow == true
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                        }
                        else
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                        }
                        break
                    case 3:
                        
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                        break
                    default:
                        break
                    }
                }

            }
            else
            {
                switch previousIndexPath.row
                {
                case 0:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                    break
                case 1:
                    if (cell.viewWithTag(11) as! UILabel).text == "Add To MyList"
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList")
                    }
                    break
                case 2:
                    if tvshow == true
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                    }
                    break
                case 3:
                    
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like")
                    break
                default:
                    break
                }
            }
        }
        // next focus view
        if let nextIndexPath = context.nextFocusedIndexPath,
            let cell = tableView.cellForRow(at: nextIndexPath)
        {
            (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
            if TvshowPath["watchedVideo"] != nil
            {
                if  TvshowPath["watchedVideo"] as! Float64 > 0.0
                {
                    switch nextIndexPath.row
                    {
                    case 0:
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1_hover")
                        break
                    case 1:
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1_hover")
                        break
                    case 2:
                        if (cell.viewWithTag(11) as! UILabel).text == "Add To MyList"
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist_hover")
                        }
                        else
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList_hover")
                        }
                        break
                    case 3:
                        if tvshow == true
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                        }
                        else
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like_gray")
                        }
                        break
                    case 4:
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like_gray")
                        break
                    default:
                        break
                    }
                }
                else
                {
                    switch nextIndexPath.row
                    {
                    case 0:
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1_hover")
                        break
                    case 1:
                        if (cell.viewWithTag(11) as! UILabel).text == "Add To MyList"
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist_hover")
                        }
                        else
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList_hover")
                        }
                        break
                    case 2:
                        if tvshow == true
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                        }
                        else
                        {
                            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like_gray")
                        }
                        break
                    case 3:
                        
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like_gray")
                        break
                    default:
                        break
                    }
                }
            }
          
            else
            {
                switch nextIndexPath.row
                {
                case 0:
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1_hover")
                    break
                case 1:
                    if (cell.viewWithTag(11) as! UILabel).text == "Add To MyList"
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist_hover")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList_hover")
                    }
                    break
                case 2:
                    if tvshow == true
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like_gray")
                    }
                    break
                case 3:
                    
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "like_gray")
                    break
                default:
                    break
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let PlayerVC = storyBoard.instantiateViewController(withIdentifier: "playerLayer") as! PlayerLayerViewController
        let subscriptionPage = storyBoard.instantiateViewController(withIdentifier: "subscription") as! SubscriptionViewController
      //  let paired = getaccountDetails()
        if TvshowPath["watchedVideo"] != nil
        {
            if  TvshowPath["watchedVideo"] as! Float64 > 0.0
            {
                switch indexPath.row
                {
                case 0:
                    //                if (TvshowPath["subscription_status"] as! String) == "active"
                    //                {
                    PlayerVC.isResume = true
                    if TvshowPath["url_m3u8"] != nil && TvshowPath["url_m3u8"] as! String != ""
                    {
                        PlayerVC.videoUrl = TvshowPath["url_m3u8"] as! String
                    }
                    else
                    {
                        PlayerVC.videoUrl = TvshowPath["url"] as! String
                    }
                    PlayerVC.mainVideoID = TvshowPath["id"] as! String
                    guard let rtime = TvshowPath["watchedVideo"] as? Float64 else {
                        return  PlayerVC.resumeTime = 0.0
                    }
                    PlayerVC.resumeTime = rtime
                    //  PlayerVC.resumeTime = (TvshowPath["watchedVideo"] as! Float64)
                    PlayerVC.deviceID = self.deviceId
                    PlayerVC.isMyList = self.isMyList
                    PlayerVC.userID = self.userid
                    PlayerVC.videoID = (TvshowPath["id"] as! String)
                    
                    
                    // self.navigationController?.pushViewController(PlayerVC, animated: true)
                    if fromSearch == true
                    {
                        PlayerVC.issearch = true
                        if ispaired
                        {
                            self.present(PlayerVC, animated: true, completion: nil)
                        }
                        else
                        {
                            pairingPage()
                        }
                        //                        if (TvshowPath["status"] as! Bool) == false
                        //                        {
                        //                            pairingPage()
                        //                        }
                        //                        else
                        //                        {
                        //                            if paired
                        //                            {
                        //
                        //                            }
                        //                            else
                        //                            {
                        //                                pairingPage()
                        //                            }
                        //
                        //                        }
                        
                    }
                    else
                    {
                        PlayerVC.issearch = false
                        if ispaired
                        {
                            self.navigationController?.pushViewController(PlayerVC, animated: true)
                        }
                        else
                        {
                            pairingPage()
                        }
                        
                        
                        //                        if (TvshowPath["status"] as! Bool) == false
                        //                        {
                        //                           pairingPage()
                        //                        }
                        //                        else
                        //                        {
                        //
                        //                        }
                        
                    }
                    
                    //                }
                    //                else
                    //                {
                    //                    self.navigationController?.pushViewController(subscriptionPage, animated: true)
                    //                }
                    
                    break
                case 1:
                    //                if (TvshowPath["subscription_status"] as! String) == "active"
                    //                {
                    RecentlyWatched(withurl:kRecentlyWatchedUrl)
                    if TvshowPath["url_m3u8"] != nil && TvshowPath["url_m3u8"] as! String != ""
                    {
                        PlayerVC.videoUrl = TvshowPath["url_m3u8"] as! String
                    }
                    else
                    {
                        PlayerVC.videoUrl = TvshowPath["url"] as! String
                    }
                    PlayerVC.mainVideoID = TvshowPath["id"] as! String
                    PlayerVC.deviceID = self.deviceId
                    PlayerVC.isMyList = self.isMyList
                    PlayerVC.userID = self.userid
                    PlayerVC.videoID = (TvshowPath["id"] as! String)
                    if fromSearch == true
                    {
                        PlayerVC.issearch = true
                        //                        if (TvshowPath["status"] as! Bool) == false
                        //                        {
                        //                           pairingPage()
                        //                        }
                        //                        else
                        //                        {
                        //                            if paired
                        //                            {
                        //                                self.present(PlayerVC, animated: true, completion: nil)
                        //                            }
                        //                            else
                        //                            {
                        //                                pairingPage()
                        //                            }
                        //
                        //                        }
                        if ispaired
                        {
                            self.present(PlayerVC, animated: true, completion: nil)
                        }
                        else
                        {
                            pairingPage()
                        }
                        
                        
                    }
                    else
                    {
                        PlayerVC.issearch = false
                        if ispaired
                        {
                            self.navigationController?.pushViewController(PlayerVC, animated: true)
                        }
                        else
                        {
                            pairingPage()
                        }
                        
                        //                        if (TvshowPath["status"] as! Bool) == false
                        //                        {
                        //                            pairingPage()
                        //                        }
                        //                        else
                        //                        {
                        //                            if paired
                        //                            {
                        //                                self.navigationController?.pushViewController(PlayerVC, animated: true)
                        //                            }
                        //                            else
                        //                            {
                        //                               pairingPage()
                        //                            }
                        //                        }
                        
                    }
                    
                    //  self.navigationController?.pushViewController(PlayerVC, animated: true)
                    
                    
                    //                }
                    //                else
                    //                {
                    //                    self.navigationController?.pushViewController(subscriptionPage, animated: true)
                    //                }
                    break
                case 2:
                    if ispaired
                    {
                        ManageList(withurl:kManageListUrl,forPath:indexPath,assetid:(TvshowPath["id"] as! String))
                    }
                    else
                    {
                        pairingPage()
                    }
                    //
                    //                if TvshowPath["status"] as! Bool == true
                    //                {
                    //
                    //                }
                    //                else
                    //                {
                    //                                  }
                    
                    break
                case 3:
                    if tvshow
                    {
                        //                    if ispaired
                        //                    {
                        //                        let StoryBoard = self.storyboard?.instantiateViewController(withIdentifier: "Episodes") as! EpisodesViewController
                        //                        // StoryBoard.contains = TvshowPath["contains"] as! NSDictionary
                        //                        StoryBoard.subcontains = TvshowPath["contains"] as! NSArray
                        //                        StoryBoard.UserID = self.userid
                        //                        StoryBoard.videoId = TvshowPath["id"] as! String
                        //                        StoryBoard.navigationController?.isNavigationBarHidden = true
                        //                        StoryBoard.episodeDelegate = delegate
                        //                        StoryBoard.isfromAlleyez = true
                        //                        StoryBoard.deviceId = self.deviceId
                        //                        StoryBoard.CarousalName = (TvshowPath["name"] as! String)
                        let arr = NSMutableArray()
                        arr.add((TvshowPath["name"] as! String))
                        
                        let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
                        let CategoriesPage = storyBoard.instantiateViewController(withIdentifier: "Categories") as! CategoriesViewController
                        // let tableview:UITableView = self.superview?.superview as! UITableView
                        //   let viewcontroller = tableview.dataSource as! MainViewController
                        CategoriesPage.CategoryCarousalName = arr
                        CategoriesPage.CarousalData = (TvshowPath["contains"] as! NSArray) as! [[String : Any]]
                        CategoriesPage.UserId = self.userid
                        CategoriesPage.uuid = self.uuid
                        CategoriesPage.deviceId = self.deviceId
                        CategoriesPage.fromDetailPage = true
                        //   CategoriesPage.fromsearch = true
                        CategoriesPage.categoryDelegate = delegate
                        
                        //   CategoriesPage.MyListthumb = viewcontroller.myList
                        //   CategoriesPage.PhotoDict = viewcontroller.PhotoDict
                        //    CategoriesPage.storeProdData = viewcontroller.storeProdData
                        //   CategoriesPage.categoryDelegate = viewcontroller
                        //     self.navigationController?.pushViewController(CategoriesPage, animated: true)
                        
                        if self.fromSearch
                        {
                            CategoriesPage.fromsearch = true
                            self.present(CategoriesPage, animated: true, completion: nil)
                        }
                        else
                        {
                            CategoriesPage.fromsearch = false
                            self.navigationController?.pushViewController(CategoriesPage, animated: false)
                        }
                        
                        //                    }
                        //                    else
                        //                    {
                        //                       pairingPage()
                        //                    }
                        
                    }
                    else
                    {
                        
                        
                    }
                    break
                case 4:
                    break
                default:
                    break
                }
            }
            else
            {
                switch indexPath.row
                {
                case 0:
                    //                if (TvshowPath["subscription_status"] as! String) == "active"
                    //                {
                    if (TvshowPath["status"] as! Bool) == true
                    {
                        RecentlyWatched(withurl:kRecentlyWatchedUrl)
                    }
                    if TvshowPath["url_m3u8"] != nil && TvshowPath["url_m3u8"] as! String != ""
                    {
                        PlayerVC.videoUrl = TvshowPath["url_m3u8"] as! String
                        
                        
                    }
                    else
                    {
                        PlayerVC.videoUrl = TvshowPath["url"] as! String
                    }
                    
                    PlayerVC.mainVideoID = TvshowPath["id"] as! String
                    PlayerVC.userID = userid
                    PlayerVC.videoID = (TvshowPath["id"] as! String)
                    PlayerVC.deviceID = deviceId
                    PlayerVC.isMyList = isMyList
                    if fromSearch == true
                    {
                        PlayerVC.issearch = true
                        var parameters =  [String:[String:AnyObject]]()
                        parameters = ["getAccountInfo":["deviceId":deviceId as AnyObject,"uuid":uuid as AnyObject]]
                        
                        ApiManager.sharedManager.postDataWithJson(url: kAccountInfoUrl, parameters: parameters){
                            (responseDict,error,isDone) in
                            if error == nil
                            {
                                let Json = responseDict
                                let dict = Json as! NSDictionary
                                let pair = (dict.value(forKey: "uuid_exist") as! Bool)
                                if pair
                                {
                                    self.present(PlayerVC, animated: true, completion: nil)
                                }
                                else
                                {
                                    self.pairingPage()
                                }
                                
                            }
                            else
                            {
                                
                            }
                        }
                        
                    }
                    else
                    {
                        PlayerVC.issearch = false
                        
                        if ispaired
                        {
                            self.navigationController?.pushViewController(PlayerVC, animated: true)
                            
                        }
                        else
                        {
                            pairingPage()
                            
                        }
                        
                    }
                    
                    break
                case 1:
                    
                    if ispaired
                    {
                        ManageList(withurl:kManageListUrl,forPath:indexPath,assetid:(TvshowPath["id"] as! String))
                        
                    }
                    else
                    {
                        pairingPage()
                    }
                    break
                case 2:
                    if tvshow == true
                    {
                        //                    if ispaired
                        //                    {
                        //                        let StoryBoard = self.storyboard?.instantiateViewController(withIdentifier: "Episodes") as! EpisodesViewController
                        //
                        //                        //   StoryBoard.contains = TvshowPath["contains"] as! NSDictionary
                        //                        StoryBoard.subcontains = TvshowPath["contains"] as! NSArray
                        //                        StoryBoard.UserID = self.userid
                        //                        StoryBoard.videoId = TvshowPath["id"] as! String
                        //                        StoryBoard.episodeDelegate = delegate
                        //                        StoryBoard.isfromAlleyez = true
                        //                        StoryBoard.CarousalName = (TvshowPath["name"] as! String)
                        //                        StoryBoard.deviceId = self.deviceId
                        //                        StoryBoard.navigationController?.isNavigationBarHidden = true
                        
                        
                        let arr = NSMutableArray()
                        arr.add((TvshowPath["name"] as! String))
                        
                        let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
                        let CategoriesPage = storyBoard.instantiateViewController(withIdentifier: "Categories") as! CategoriesViewController
                        // let tableview:UITableView = self.superview?.superview as! UITableView
                        //   let viewcontroller = tableview.dataSource as! MainViewController
                        CategoriesPage.CategoryCarousalName = arr
                        CategoriesPage.CarousalData = (TvshowPath["contains"] as! NSArray) as! [[String : Any]]
                        CategoriesPage.UserId = self.userid
                        CategoriesPage.uuid = self.uuid
                        CategoriesPage.deviceId = self.deviceId
                        CategoriesPage.fromDetailPage = true
                        
                        CategoriesPage.categoryDelegate = delegate
                        
                        
                        
                        if self.fromSearch
                        {
                            CategoriesPage.fromsearch = true
                            self.present(CategoriesPage, animated: true, completion: nil)
                        }
                        else
                        {
                            CategoriesPage.fromsearch = false
                            self.navigationController?.pushViewController(CategoriesPage, animated: false)
                        }
                        //  }
                        //                    else
                        //                    {
                        //                        pairingPage()
                        //                    }
                        
                        //  self.present(StoryBoard, animated: true, completion: nil)
                    }
                    else
                    {
                        
                    }
                    
                    break
                case 3:
                    
                    break
                default:
                    break
                }
            }

        }
       
        else
        {
            switch indexPath.row
            {
            case 0:
//                if (TvshowPath["subscription_status"] as! String) == "active"
//                {
                    if (TvshowPath["status"] as! Bool) == true
                    {
                        RecentlyWatched(withurl:kRecentlyWatchedUrl)
                    }
                    if TvshowPath["url_m3u8"] != nil && TvshowPath["url_m3u8"] as! String != ""
                    {
                        PlayerVC.videoUrl = TvshowPath["url_m3u8"] as! String
                 

                    }
                    else
                    {
                        PlayerVC.videoUrl = TvshowPath["url"] as! String
                    }
                   
                    PlayerVC.mainVideoID = TvshowPath["id"] as! String
                    PlayerVC.userID = userid
                    PlayerVC.videoID = (TvshowPath["id"] as! String)
                    PlayerVC.deviceID = deviceId
                    PlayerVC.isMyList = isMyList
                    if fromSearch == true
                    {
                        PlayerVC.issearch = true
                        var parameters =  [String:[String:AnyObject]]()
                        parameters = ["getAccountInfo":["deviceId":deviceId as AnyObject,"uuid":uuid as AnyObject]]
                        
                        ApiManager.sharedManager.postDataWithJson(url: kAccountInfoUrl, parameters: parameters){
                            (responseDict,error,isDone) in
                            if error == nil
                            {
                                let Json = responseDict
                                let dict = Json as! NSDictionary
                                let pair = (dict.value(forKey: "uuid_exist") as! Bool)
                                if pair
                                {
                                   self.present(PlayerVC, animated: true, completion: nil)
                                }
                                else
                                {
                                     self.pairingPage()
                                }
                                
                            }
                            else
                            {
                                
                            }
                        }

                    }
                    else
                    {
                        PlayerVC.issearch = false
                        
                        if ispaired
                        {
                            self.navigationController?.pushViewController(PlayerVC, animated: true)

                        }
                        else
                        {
                            pairingPage()

                        }
                        
                    }

                break
            case 1:

                if ispaired
                {
                    ManageList(withurl:kManageListUrl,forPath:indexPath,assetid:(TvshowPath["id"] as! String))

                }
                else
                {
                    pairingPage()
                }
                break
            case 2:
                if tvshow == true
                {
//                    if ispaired
//                    {
//                        let StoryBoard = self.storyboard?.instantiateViewController(withIdentifier: "Episodes") as! EpisodesViewController
//                        
//                        //   StoryBoard.contains = TvshowPath["contains"] as! NSDictionary
//                        StoryBoard.subcontains = TvshowPath["contains"] as! NSArray
//                        StoryBoard.UserID = self.userid
//                        StoryBoard.videoId = TvshowPath["id"] as! String
//                        StoryBoard.episodeDelegate = delegate
//                        StoryBoard.isfromAlleyez = true
//                        StoryBoard.CarousalName = (TvshowPath["name"] as! String)
//                        StoryBoard.deviceId = self.deviceId
//                        StoryBoard.navigationController?.isNavigationBarHidden = true
                        
                        
                        let arr = NSMutableArray()
                        arr.add((TvshowPath["name"] as! String))
                        
                        let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
                        let CategoriesPage = storyBoard.instantiateViewController(withIdentifier: "Categories") as! CategoriesViewController
                        // let tableview:UITableView = self.superview?.superview as! UITableView
                        //   let viewcontroller = tableview.dataSource as! MainViewController
                        CategoriesPage.CategoryCarousalName = arr
                        CategoriesPage.CarousalData = (TvshowPath["contains"] as! NSArray) as! [[String : Any]]
                        CategoriesPage.UserId = self.userid
                        CategoriesPage.uuid = self.uuid
                        CategoriesPage.deviceId = self.deviceId
                        CategoriesPage.fromDetailPage = true
                
                        CategoriesPage.categoryDelegate = delegate


                    
                        if self.fromSearch
                        {
                             CategoriesPage.fromsearch = true
                            self.present(CategoriesPage, animated: true, completion: nil)
                        }
                        else
                        {
                             CategoriesPage.fromsearch = false
                            self.navigationController?.pushViewController(CategoriesPage, animated: false)
                        }
                  //  }
//                    else
//                    {
//                        pairingPage()
//                    }
               
                  //  self.present(StoryBoard, animated: true, completion: nil)
                }
                else
                {
                    
                }
              
                break
            case 3:
                
                break
            default:
                break
            }
        }
        PlayerVC.detdelegate = self
    }
    func getaccountDetails()
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAccountInfo":["deviceId":deviceId as AnyObject,"uuid":uuid as AnyObject]]
    
        ApiManager.sharedManager.postDataWithJson(url: kAccountInfoUrl, parameters: parameters){
            (responseDict,error,isDone) in
            if error == nil
            {
                let Json = responseDict
                let dict = Json as! NSDictionary
                self.ispaired = (dict.value(forKey: "uuid_exist") as! Bool)
              
            }
            else
            {
                
            }
        }
      
    }
    
    // Finding TVShow or not
    func getTvShowCount(isTvShow:Bool)->Int
    {
        
        Menus.removeAll()
        if TvshowPath["watchedVideo"] != nil
        {
            if  TvshowPath["watchedVideo"] as! Float64 > 0.0
            {
                Menus.append("Resume playing")
            }
        }
       
        if isTvShow
        {
            Menus.append("Play")
            if TvshowPath["myList"] as! Bool == false
            {
                Menus.append("Add To MyList")
            }
            else
            {
                Menus.append("Remove from MyList ")
            }
            Menus.append("More \(TvshowPath["name"] as! String)")
            Menus.append("Rate this title")
        }
        else
        {
            Menus.append("Play")
            if TvshowPath["myList"] as! Bool == false
            {
                Menus.append("Add To MyList")
            }
            else
            {
                Menus.append("Remove from MyList ")
            }
            Menus.append("Rate this title")
        }
        return Menus.count
    }
    
    func pairingPage()
    {
        var parameters = [String:[String:AnyObject]]()
        parameters = ["getActivationCode":["model":"AppleTV4Gen" as AnyObject,"manufacturer":"Apple" as AnyObject,"device_name":"AppleTV" as AnyObject, "device_id":deviceId! as AnyObject,"device_mac_address":"mac" as AnyObject,"brand_name":"Apple" as AnyObject,"host_name":"app" as AnyObject,"display_name":"apple" as AnyObject, "serial_number":"1234" as AnyObject,"version":"AEOM" as AnyObject,"platform":"TVOS" as AnyObject]]
        ApiManager.sharedManager.postDataWithJson(url: kActivationCodeUrl, parameters: parameters) { (responseDict, error,isDone)in
            if error == nil
            {
                let dict = responseDict as! [String:String]
                self.uuid = dict["uuid"]
                self.deviceId = dict["device_id"]
    
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                //                                    if !((dict["pair_devce"]! as String).contains("active"))
                //                                    {
                //    let rootView = storyBoard.instantiateViewController(withIdentifier: "Code") as! CodeViewController
                let rootView = storyBoard.instantiateViewController(withIdentifier: "PPCode") as! PPCodeViewController
                rootView.uuid = self.uuid
                rootView.deviceId = self.deviceId
                rootView.code = dict["code"]
                if self.fromSearch
                {
                    self.present(rootView, animated: false, completion: nil)
                }
                else
                {
                self.navigationController?.pushViewController(rootView, animated: true)
                }
                // }
            }
        }
    }
    // Service Call for Create Recently Watched
    func RecentlyWatched(withurl:String)
    {
        let parameters = ["createRecentlyWatched": ["videoId":TvshowPath["id"]! as AnyObject,"userId":userid as AnyObject,"carouselName":"All Eyez On Me" as AnyObject]]
        ApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters){(responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict
                let dict = JSON as! NSDictionary
                let dicton = dict["recentlyWatched"] as! NSDictionary
                if (dicton["seekTime"] as! Float64) > 0
                {
                    self.isRecentlywatch = true
                }
                self.delegate?.recentlyWatcheddata(recentlyWatchedDict: dicton)
            }
            else
            {
              //  print("json error")
                print(error)
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                let _ = self.navigationController?.popViewController(animated: true)
//                })
//                alertview.addAction(defaultAction)
//                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    // Service call for ManageMyList
    func ManageList(withurl:String,forPath:IndexPath,assetid:String)
    {
        let parameters = ["manageMyList":["videoId":(TvshowPath["id"])!as AnyObject,"userId":userid as AnyObject,"data":TvshowPath as AnyObject,"carouselName":"All Eyez On Me" as AnyObject]]
        ApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters){(responseDict, error,isDone) in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                let dict = JSON["myList"] as! NSDictionary
                let cell = self.operationsListView.cellForRow(at: forPath)
                if (dict["data"] != nil)
                {
                    (cell?.viewWithTag(11) as! UILabel).text = "Remove from MyList"
                    (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList_hover")
                    self.delegate?.myListdata(mylistDict: dict)
                }
                else
                {
                    (cell?.viewWithTag(11) as! UILabel).text = "Add To MyList"
                    (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist_hover")
                    self.delegate?.removeListdata(id: assetid)
                }
                self.getassetdata(withUrl:kAssestDataUrl,id:((self.TvshowPath["id"])!as AnyObject) as! String,userid:(self.userid as AnyObject) as! String)
            }
            else
            {
                print("json error")
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                let _ = self.navigationController?.popViewController(animated: true)
//                })
//                alertview.addAction(defaultAction)
//                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
   /* func getAssetData(withUrl:String,id:String,userid:String)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAssestData":["videoId":id as AnyObject,"userId":userid as AnyObject,"returnType":"tiny" as AnyObject]]
        ApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                let jsonresponse = JSON as! NSArray
                for dict in jsonresponse
                {
                    self.TvshowPath = dict as! NSDictionary
                }
            }
            else
            {
                print("json Error")
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
            self.operationsListView.reloadData()
        }
    }*/
    
    func accountStatus()
    {
        let url = kAccountInfoUrl
        
        let  parameters = [ "getAccountInfo": ["deviceId": self.deviceId!, "uuid": self.uuid!]]
            ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters as [String : [String : AnyObject]]) {(responseDict , error,isDone) in
            if error == nil
            {
                let post = responseDict
                self.statusDict = post as! NSDictionary
                //                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                //                let accountInfo = storyBoard.instantiateViewController(withIdentifier: "Account") as! AccountInfoViewController
                //                accountInfo.accountDict = dict
                //                self.navigationController?.pushViewController(accountInfo, animated: true)
            }
            else
            {
                print("json error")
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                  let _ = self.navigationController?.popViewController(animated: true)
//                })
//                alertview.addAction(defaultAction)
//                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callwatchlist(dict: NSDictionary) {
        self.delegate?.recentlyWatcheddata(recentlyWatchedDict: dict)
    }
    
    func getassetdata(withUrl:String,id:String,userid:String)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAssestData":["videoId":id as AnyObject,"userId":userid as AnyObject,"returnType":"tiny" as AnyObject,"carouselName":"All Eyez On Me" as AnyObject]]
        
        ApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                if JSON is NSArray
                {
                    let dict = JSON as! NSArray
                    self.TvshowPath = dict.firstObject as! NSDictionary
                }
                else
                {
                    self.TvshowPath = JSON as! NSDictionary
                }
           //     self.operationsListView.reloadData()
                self.viewWillAppear(true)
                
            }
            else
            {
                print("json Error")
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                    let _ = self.navigationController?.popViewController(animated: true)
//                })
//                alertview.addAction(defaultAction)
//                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
}




